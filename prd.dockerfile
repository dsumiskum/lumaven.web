FROM microsoft/dotnet:latest

MAINTAINER Dylan Sumiskum

RUN apt-get -qq update && apt-get -qqy --no-install-recommends install \
    git \
    unzip


# install npm
RUN curl -sL https://deb.nodesource.com/setup_6.x |  bash -
RUN apt-get install -y nodejs

# install bower
RUN ["npm", "install", "--global", "bower"]

# install gulp
RUN ["npm", "install", "--global", "gulp"]

# install gulp-ruby-sass
RUN ["npm", "install", "--global", "gulp-ruby-sass"]

# install ruby and gem-sass
RUN apt-get install -y ruby-full rubygems-integration
RUN ["gem", "install", "sass"]

# mount source files
COPY . /app

# build projects
WORKDIR /app/src/Data
RUN ["dotnet", "restore"]

WORKDIR /app/src/Web
RUN ["dotnet", "restore"]
RUN ["npm", "install"]
RUN ["bower", "install", "--allow-root"]

# setup environment
ENV ASPNETCORE_URLS http://*:80
ENV ASPNETCORE_ENVIRONMENT="prd"
EXPOSE 80

# image entrypoint
ENTRYPOINT ["dotnet", "run"]
