﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Data.Mongo
{
    public class MongoRepository<TEntity, TKey> : IRepository<TEntity, TKey>
    {
        private readonly IMongoCollection<TEntity> _collection;

        public MongoRepository(string connectionString, string databaseName, string collectionName)
            : this(new MongoClient(connectionString), databaseName, collectionName)
        {
        }

        public MongoRepository(IMongoClient client, string databaseName, string collectionName) 
            : this(client.GetDatabase(databaseName).GetCollection<TEntity>(collectionName))
        {
        }

        public MongoRepository(IMongoCollection<TEntity> collection)
        {
            _collection = collection;
        }

        #region IQueryable implementations
        public Type ElementType
        {
            get
            {
                return _collection.AsQueryable<TEntity>().ElementType;
            }
        }

        public Expression Expression
        {
            get
            {
                return _collection.AsQueryable<TEntity>().Expression;
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                return _collection.AsQueryable<TEntity>().Provider;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _collection.AsQueryable<TEntity>().GetEnumerator();
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return _collection.AsQueryable<TEntity>().GetEnumerator();
        }
        #endregion

        public TEntity Create(TEntity entity)
        {
            _collection.InsertOne(entity);

            return entity;
        }

        public void Delete(TKey id)
        {
            _collection.DeleteOne(ObjectIdFilter(id));
        }

        public TEntity Get(TKey id)
        {
            return _collection.Find(ObjectIdFilter(id)).First();
        }

        public bool TryGet(TKey id, out TEntity entity)
        {
            entity = _collection.Find(ObjectIdFilter(id)).FirstOrDefault();

            return entity != null;
        }

        public TEntity Update(TKey id, TEntity entity)
        {
            _collection.ReplaceOne(ObjectIdFilter(id), entity, new UpdateOptions { IsUpsert = false });

            return entity;
        }

        public TEntity Upsert(TKey id, TEntity entity)
        {
            _collection.ReplaceOne(ObjectIdFilter(id), entity, new UpdateOptions { IsUpsert = true });

            return entity;
        }

        #region private helper methods
        private FilterDefinition<TEntity> ObjectIdFilter(TKey id)
        {
            return Builders<TEntity>.Filter.Eq("_id", id.ToString()); ;
        }
        #endregion
    }
}
