﻿using System.Linq;

namespace Data
{
    public interface IRepository<TEntity, TKey> : IQueryable<TEntity>
    {
        /// <summary>
        /// Creates new entity of type TEntity
        /// </summary>
        /// <param name="entity">The entity to create.</param>
        /// <returns>Created entity.</returns>
        TEntity Create(TEntity entity);

        /// <summary>
        /// Returns entity of type TEntity by its given id of type TKey
        /// </summary>
        /// <param name="id">The unique id of the entity of type TKey</param>
        /// <returns></returns>
        TEntity Get(TKey id);

        /// <summary>
        /// Outputs entity of type TEntity by its given id of type TKey
        /// </summary>
        /// <param name="id">The unique id of the entity of type TKey</param>
        /// <param name="entity">Output entity</param>
        /// <returns>True if found</returns>
        bool TryGet(TKey id, out TEntity entity);

        /// <summary>
        /// Update an entity
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns>Updated entity.</returns>
        TEntity Update(TKey id, TEntity entity);

        /// <summary>
        /// Create or update an entity
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        TEntity Upsert(TKey id, TEntity entity);

        /// <summary>
        /// Delete an entity by its id
        /// </summary>
        /// <param name="id"></param>
        void Delete(TKey id);
    }
}
