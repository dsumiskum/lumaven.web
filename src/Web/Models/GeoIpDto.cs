﻿namespace Web.Models
{
    public class GeoIpDto
    {
        public string ip { get; set; }
        public string country_code { get; set; }
        public string region_code { get; set; }
        public string city { get; set; }
        public string zip_code { get; set; }
    }
}
