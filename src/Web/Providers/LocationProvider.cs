﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using Web.Models;

namespace Web.Providers
{
    public class LocationProvider
    {
        private readonly string _requestUri;

        public LocationProvider()
        {
            _requestUri = "http://freegeoip.net/";
        }

        public GeoIpDto GetLocation(string remoteIpAddress)
        {
            string requestUri = $"{_requestUri}json/{remoteIpAddress}";
            HttpWebRequest request = WebRequest.Create(requestUri) as HttpWebRequest;
            using (HttpWebResponse response = request.GetResponseAsync().Result as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception($"Server error from {_requestUri} (HTTP {response.StatusCode}: {response.StatusDescription}).");
                }

                Stream sr = response.GetResponseStream();
                using (StreamReader reader = new StreamReader(sr, Encoding.UTF8))
                {
                    string content = reader.ReadToEnd();
                    var responseObject = JsonConvert.DeserializeObject<GeoIpDto>(content);

                    return responseObject;
                }
            }
        }
    }
}
