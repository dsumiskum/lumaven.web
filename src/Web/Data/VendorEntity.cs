﻿using System.Collections.Generic;

namespace Web.Data
{
    public class VendorEntity : SubscriberEntity
    {
        public VendorEntity(string email, string region) : base(email, region)
        {
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal HourlyRate { get; set; }
        public bool IsFreelancer { get; set; }
        public string CompanyName { get; set; }
        public int CompanyFounded { get; set; }
        public bool WantsToBlog { get; set; }
        public IEnumerable<string> Services { get; set; }
    }
}
