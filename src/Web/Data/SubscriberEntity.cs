﻿using MongoDB.Bson.Serialization.Attributes;

namespace Web.Data
{
    public class SubscriberEntity
    {
        public SubscriberEntity(string email, string countryCode)
        {
            Email = email;
            CountryCode = countryCode;
        }

        [BsonId]
        public string Email { get; set; }
        public string IpAddress { get; set; }
        public string CountryCode { get; set; }
        public string RegionCode { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
    }
}
