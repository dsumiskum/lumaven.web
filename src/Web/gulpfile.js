﻿var gulp = require('gulp')
var sass = require('gulp-ruby-sass')
var bundler = require('browserify')
var source = require('vinyl-source-stream')
var rename = require('gulp-rename')


gulp.task('views', function () {
	gulp.src('./app/**/*.html')
        .pipe(rename({ dirname: '' }))
        .pipe(gulp.dest('./wwwroot/'))
})

gulp.task('bundle', function () {
	return bundler('./app/app.js')
           .bundle()
           .pipe(source('main.js'))
           .pipe(gulp.dest('./wwwroot/js/'))
})

gulp.task('watch', function () {
	gulp.watch('app/**/*.js', ['bundle'])
	gulp.watch('app/**/*.html', ['views'])
	gulp.watch('sass/*.scss', ['sass'])
})

gulp.task('sass', function () {
	return sass('sass/style.scss')
        .pipe(gulp.dest('wwwroot/css'))
})

gulp.task('build', ['bundle', 'views', 'sass'])

gulp.task('default', ['build', 'watch'])