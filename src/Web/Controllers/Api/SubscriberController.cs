﻿using Microsoft.AspNetCore.Mvc;
using Web.Models;
using Web.Data;
using Web.Providers;
using Data;
using Microsoft.Extensions.Logging;
using System;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers
{
    [Route("api/[controller]")]
    public class SubscriberController : Controller
    {
        private readonly IRepository<SubscriberEntity, string> _subscriberRepository;
        private readonly LocationProvider _locationProvider;
        private readonly ILogger<SubscriberController> _logger;

        public SubscriberController(IRepository<SubscriberEntity, string> subscriberRepository, LocationProvider locationProvider, ILogger<SubscriberController> logger)
        {
            _subscriberRepository = subscriberRepository;
            _locationProvider = locationProvider;
            _logger = logger;
        }

        // POST api/values
        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Post([FromBody]EmailDto email)
        {
            if (email == null || string.IsNullOrEmpty(email.Email))
            {
                return BadRequest();
            }

            try
            {
                var remoteIpAddress = Request.Headers["IpAddress"];
                var location = _locationProvider.GetLocation(remoteIpAddress);

                var newSubscriber = new SubscriberEntity(email.Email.ToLower(), location.country_code)
                {
                    IpAddress = remoteIpAddress,
                    CountryCode = location.country_code,
                    RegionCode = location.region_code,
                    City = location.city,
                    ZipCode = location.zip_code
                };

                _subscriberRepository.Upsert(email.Email, newSubscriber);

                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
