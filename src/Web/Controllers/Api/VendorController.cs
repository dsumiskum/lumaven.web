﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Web.Providers;
using Web.Data;
using Microsoft.Extensions.Logging;
using Data;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class VendorController : Controller
    {
        private readonly IRepository<VendorEntity, string> _vendorRepository;
        private readonly LocationProvider _locationProvider;
        private readonly ILogger<VendorController> _logger;

        public VendorController(IRepository<VendorEntity, string> vendorRepository, LocationProvider locationProvider, ILogger<VendorController> logger)
        {
            _vendorRepository = vendorRepository;
            _locationProvider = locationProvider;
            _logger = logger;
        }

        [HttpGet, ValidateAntiForgeryToken]
        [Route("unique/{email}")]
        public IActionResult Unique(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return BadRequest();
            }

            try
            {
                VendorEntity vendor;
                bool isUnique = !_vendorRepository.TryGet(email.ToLower(), out vendor);

                return new ObjectResult(new { isUnique = isUnique });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        // POST api/values
        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult Post([FromBody]JObject formData)
        {
            try
            {
                var remoteIpAddress = Request.Headers["IpAddress"];
                var location = _locationProvider.GetLocation(remoteIpAddress);

                var emailToken = formData["email"]?.ElementAtOrDefault(0);
                var firstNameToken = formData["fullname"]?.ElementAtOrDefault(0);
                var lastNameToken = formData["fullname"]?.ElementAtOrDefault(1);
                var hourlyRateToken = formData["hourlyRate"]?.ElementAtOrDefault(0);
                var isFreelancerToken = formData["isFreelancer"]?.ElementAtOrDefault(0);
                var servicesOfferedToken = formData["servicesOffered"];
                var wantsToBlogToken = formData["wantsToBlog"]?.ElementAtOrDefault(0);
                var firmNameToken = formData["firmName"]?.ElementAtOrDefault(0);
                var firmFoundedToken = formData["firmFounded"]?.ElementAtOrDefault(0);

                var email = emailToken.Value<string>();

                if (emailToken == null || isFreelancerToken == null || wantsToBlogToken == null || servicesOfferedToken == null)
                {
                    return BadRequest();
                }

                var isFreelancer = isFreelancerToken.Value<string>().Equals("freelancer", StringComparison.CurrentCultureIgnoreCase) ? true : false;
                var wantsToBlog = wantsToBlogToken.Value<string>().Equals("Yes", StringComparison.CurrentCultureIgnoreCase) ? true : false;
                var servicesOffered = (JArray)servicesOfferedToken;
                var services = servicesOffered.Select(s => (string)s).ToList();

                var vendorEntity = new VendorEntity(email.ToLower(), location.country_code)
                {
                    FirstName = firstNameToken?.Value<string>(),
                    LastName = lastNameToken?.Value<string>(),
                    CompanyName = firmNameToken?.Value<string>(),
                    CompanyFounded = firmFoundedToken?.Value<int>() ?? 0,
                    IsFreelancer = isFreelancer,
                    WantsToBlog = wantsToBlog,
                    HourlyRate = hourlyRateToken?.Value<decimal>() ?? 0,
                    Services = services,
                    IpAddress = remoteIpAddress,
                    CountryCode = location.country_code,
                    RegionCode = location.region_code,
                    City = location.city,
                    ZipCode = location.zip_code
                };

                _vendorRepository.Create(vendorEntity);

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(new EventId(500, "Internal Server Error"), ex, ex.Message);
                return StatusCode(500, ex);
            }
        }
    }
}
