var surveyController = [
    '$scope',
    '$document',
    '$mdMedia',
    '$timeout',
    '$q',
    '$window',
    function ($scope, $document, $mdMedia, $timeout, $q, $window) {
        var model = this; // using 'this' in javascript is confusing as it can mean different things based on the scope level

        /* ==============================
                   Controller Setup
           ============================== */

        // executed like a constructor
        model.$onInit = function () {
            /* gets the survey's container id, survey needs to be placed inside a container that has scrollable y */
            model.surveyContainer = angular.element(document.getElementById(model.containerId));

            /* the dictionary object that will contain answers to the survey questions
               the keys are the question's key so that you can correlate an answer to a question*/
            model.answers = {};

            /* a dictionary object that maps the previous question to a particular question identified by its key
               this is because we have skip logic based on a question's answer hence it makes sense to create a map to account
               for the dynamic behavior */
            model.mapPrevious = {};

            /* we retrieve the dictionary data about the questions from the survey config */
            model.questions = model.config.questions;

            /* we retrieve an array of question keys that provide information about the ordering */
            model.questionsOrder = model.config.questionsOrder;

            /* a configuration object that sets the title, elementId, and description of the intro message */
            model.prologue = model.config.prologue;

            /* a configuration object that sets the title, elementId, and description of the completion message */
            model.epilogue = model.config.epilogue;
        }

        // executed when angular has finished processing the DOM and template
        model.$postLink = function () {

            // we want to scroll the screen to the prologue on page load
            $timeout(function () {
                var element = angular.element(document.getElementById(model.config.prologue.elementId));
                model.focusedElement = element;
                scrollToElement(element);
            }, 200, true);
        }

        /* ==============================
                   Listeners
           ============================== */

        angular.element($window).bind('resize', function () {
            if (model.snapTimeout) {
                $timeout.cancel(model.snapTimeout);
            }
            model.snapTimeout = $timeout(function () {
                if (model.focusedElement) {
                    scrollToElement(model.focusedElement);
                }
            }, 300, true);
            $scope.$apply();
        });

        /* ==============================
                   Public Methods
           ============================== */

        // the function call when the prologue's get started button gets clicked
        model.getStarted = function () {
            model.moveToQuestion(0);
            model.started = true; // broadcast the survey has started
        }

        // function call when a next question button gets clicked
        model.nextQuestion = function (questionIndex, answer) {
            upsertAnswer(questionIndex, answer);
            var oldIndex = questionIndex;
            var nextIndex = getNextIndex(questionIndex, answer);
            model.moveToQuestion(nextIndex);
            model.mapPrevious[model.questionIndex] = oldIndex; // we map previous so that you can click previous question
        }

        // function call when a previous question button gets clicked
        model.previousQuestion = function () {
            model.moveToQuestion(model.mapPrevious[model.questionIndex]);
        }

        model.moveToQuestion = function (index) {
            model.questionIndex = index;

            if (model.questionIndex >= model.questionsOrder.length) {
                model.submitInProgress = true;
                // call the onCompleted callback function since the survey has completed
                // onCompleted must return a promise
                model.onCompleted({ answers: model.answers }).then(function (success) {
                    // broadcast survey has completed
                    model.completed = true;
                    model.submitInProgress = false;

                    // scroll to epilogue section
                    var element = angular.element(document.getElementById(model.epilogue.elementId));
                    model.focusedElement = element;
                    scrollToElement(element);
                }, function (error) {
                    model.submitInProgress = false;
                    console.log(error);
                }, function (update) {
                    console.log(update);
                });
            }
            else {
                // broadcast survey has not completed
                model.completed = false;

                // by default the questionKey is used as the elementId of the question DOM element
                var questionKey = model.questionsOrder[model.questionIndex];
                var element = angular.element(document.getElementById(questionKey));
                model.focusedElement = element;
                scrollToElement(element);

                // we peek ahead to figure out of the question we are moving to is the last question
                var peekIndex = getNextIndex(index);
                if (peekIndex >= model.questionsOrder.length) {
                    model.isAtLastQuestion = true;
                }
                else {
                    model.isAtLastQuestion = false;
                }
            }
        }

        /* titles can contain reference to answers from previous questions 
           the token is marked as %answers[x]% */
        model.parseTitle = function (title) {
            var answers = model.answers;
            title = title.replace(/%(.*?)%/, function (a, b) {
                var t = eval(b) ? eval(b) : undefined;
                t = t || "[waiting answer]";
                return "<code>" + t + "</code>";// remove quote.
            });
            answers = undefined; // in case garbage collection craps
            return title;
        };

        /* ==============================
                   Internal Methods
           ============================== */

        var upsertAnswer = function (questionIndex, answer) {
            var questionKey = model.questionsOrder[questionIndex];

            // we always want to store answer as an array since there are question types that support multiple answers
            var answerArray = [];
            if (answer) {
                // check if answer is an array
                if (answer.constructor === Array) {
                    answerArray = answer;
                }
                else {
                    answerArray = [
                        answer
                    ]
                }
            }

            model.answers[questionKey] = answerArray;
        }

        var getNextIndex = function (currentIndex, answer) {
            var questionKey = model.questionsOrder[currentIndex];
            var curQuestion = model.questions[questionKey];

            // evaluate question's skip logic
            if (answer in curQuestion.skipLogic) {
                var nextQuestionKey = curQuestion.skipLogic[answer];
                return model.questionsOrder.indexOf(nextQuestionKey);
            }
            else if ("*" in curQuestion.skipLogic) {
                // * means answer can be anything 
                var nextQuestionKey = curQuestion.skipLogic["*"];
                return model.questionsOrder.indexOf(nextQuestionKey);
            }

            return currentIndex + 1;
        }

        var scrollToElement = function (element) {
            model.surveyContainer.scrollToElementAnimated(element, $window.innerHeight / 2 - element.height() / 2, 300);
        }
    }];

var template = "";
template += "<div flex-offset=\"15\"";
template += "     flex-offset-xs=\"0\">";
template += "    <div id=\"{{model.prologue.elementId}}\"";
template += "         class=\"intro\"";
template += "         layout=\"row\"";
template += "         layout-xs=\"column\"";
template += "         layout-align=\"center center\"";
template += "         layout-padding";
template += "         ng-class=\"{'focused': !model.started }\">";
template += "        <div flex=\"none\">";
template += "            <i class=\"{{model.prologue.icon.class}}\"";
template += "                style=\"font-size: 100px; color: {{model.prologue.icon.color}};\"><\/i>";
template += "        <\/div>";
template += "        <div style=\"font-size: 20px; font-weight: normal;\"";
template += "             flex=\"auto\"";
template += "             flex-offset=\"5\"";
template += "             layout-padding>";
template += "            <h3>{{model.prologue.title}}<\/h3>";
template += "            <p>";
template += "                {{model.prologue.description}}";
template += "            <\/p>";
template += "            <div>";
template += "                <button type=\"button\"";
template += "                        class=\"btn btn-default btn-skinny\"";
template += "                        ng-click=\"model.getStarted()\"";
template += "                        ng-disabled=\"model.started\">";
template += "                    <b>{{model.prologue.ctaText}}<\/b>";
template += "                <\/button>";
template += "            <\/div>";
template += "        <\/div>";
template += "    <\/div>";
template += "    <question ng-repeat=\"questionKey in model.questionsOrder\"";
template += "              data=\"model.questions[questionKey]\"";
template += "              next-question=\"model.nextQuestion(questionIndex, answer)\"";
template += "              current-index=\"model.questionIndex\"";
template += "              is-last-question=\"model.isAtLastQuestion\"";
template += "              index=\"$index\"";
template += "              q-title=\"model.parseTitle(model.questions[questionKey].title)\"";
template += "              id=\"{{questionKey}}\"";
template += "              flex=\"auto\"";
template += "              layout=\"row\"";
template += "              ng-class=\"{'focused': model.questionIndex == $index }\">";
template += "    <\/question>";
template += "    <div id=\"{{model.epilogue.elementId}}\"";
template += "         layout=\"row\"";
template += "         layout-xs=\"column\"";
template += "         layout-align=\"center center\"";
template += "         layout-padding";
template += "         class=\"end\"";
template += "         ng-class=\"{'focused': model.completed }\">";
template += "        <div flex=\"none\">";
template += "            <i class=\"{{model.epilogue.icon.class}}\"";
template += "                style=\"font-size: 100px; color: {{model.epilogue.icon.color}};\"><\/i>";
template += "        <\/div>";
template += "        <div style=\"font-size: 20px; font-weight: normal;\"";
template += "             flex=\"auto\"";
template += "             flex-offset=\"5\"";
template += "             layout-padding";
template += "             layout=\"column\">";
template += "            <h3>{{model.epilogue.title}}<\/h3>";
template += "            <p>";
template += "                {{model.epilogue.description}}";
template += "            <\/p>";
template += "        <\/div>";
template += "    <\/div>";
template += "<\/div>";
template += "";
template += "<!-- FOOTER -->";
template += "<div flex";
template += "      id=\"footer\"";
template += "      ng-if=\"model.started\">";
template += "    <div layout=\"row\"";
template += "         layout-padding";
template += "         layout-align=\"center center\">";
template += "        <md-progress-linear md-mode=\"determinate\"";
template += "                            value=\"{{model.questionIndex\/model.questionsOrder.length*100}}\"";
template += "                            ng-if=\"!model.submitInProgress\">";
template += "        <\/md-progress-linear>";
template += "        <md-progress-linear md-mode=\"indeterminate\"";
template += "                            ng-if=\"model.submitInProgress\">";
template += "        <\/md-progress-linear>";
template += "        <div>";
template += "            <button ng-if=\"model.questionIndex > 0 && !model.completed && !model.submitInProgress\"";
template += "                    ng-click=\"model.previousQuestion()\"";
template += "                    type=\"button\"";
template += "                    class=\"btn btn-default\"";
template += "                    style=\"margin-left: 10px;\">";
template += "                <i class=\"fa fa-arrow-up\"";
template += "                   aria-hidden=\"true\">";
template += "                <\/i>";
template += "            <\/button>";
template += "        <\/div>";
template += "    <\/div>";
template += "<\/div>";

module.exports = {
    template: template,
    controller: surveyController,
    controllerAs: 'model',
    bindings: {
        config: '<',
        onCompleted: '&',
        containerId: '@'
    }
};
