var surveyComponent = require('./Survey.js')
var questionComponent = require('./Question.js')
var blurOnEnter = require('./BlurOnEnter.js')

angular.module('survey', ['ngMaterial', 'ngMessages', 'duScroll'])
    .component('survey', surveyComponent)
    .component('question', questionComponent)
    .directive('blurOnEnter', blurOnEnter);