var questionController = [
    '$scope',
    '$timeout',
    '$q',
    '$sce',
    '$http',
    '$mdMedia',
    '$mdDialog',
    function ($scope, $timeout, $q, $sce, $http, $mdMedia, $mdDialog) {
        var model = this; // using 'this' in javascript is confusing as it can mean different things based on the scope level

        /* ==============================
                   Controller Setup
           ============================== */
        
        // executed like a constructor
        model.$onInit = function () {
            // detect screen size
            model.xs = $mdMedia('xs');
            model.gtMd = $mdMedia('gt-md');

            // detect iOS
            model.iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

            // function call that checks if the question is in valid state
            model.isValid = function (form) {
                return !form.innerInput.$invalid && model.answer;
            }

            // setup for autocomplete and autocompletemultiple question types
            if (model.data.type == 'autocomplete' || model.data.type == 'autocompletemultiple') {
                model.newSearchNode = newSearchNode;
                model.selectedItemChange = selectedItemChange;
                model.searchTextChange = searchTextChange;
                model.querySearch = querySearch;
                model.searchText = null;

                // retrieve autocomplete data from configured source
                $http.get(model.data.source)
                    .then(function (response) {
                        model.nodes = response.data;
                    }, function (error) {
                        console.log(error);
                    });

                // determine browser compatibility
                model.enablePopoverAutocomplete = model.xs || (model.iOS && !model.gtMd);
            }

            // setup specific to autocompletemultiple question type
            if (model.data.type == 'autocompletemultiple') {
                model.answer = []; // this question type can have multiple answers
                model.selectedItem = null;

                // override is valid for autocompletemultiple question type as model.answer defaults to empty array
                model.isValid = function (form) {
                    return model.answer.length > 0;
                }

                if (!model.gtMd) {
                    model.openSearchDialog = openSearchDialog;
                }
            }

            // setup specific to fullname question type
            if (model.data.type == 'fullname') {
                model.answer = [];
                
                // override is valid for this question type as there are multiple input fields
                model.isValid = function (form) {
                    return !form.innerInput_0.$invalid && !form.innerInput_1.$invalid && model.answer.length > 0
                }
            }
        }

        /* ==============================
                   Listeners
           ============================== */
        $scope.$watch(function () { return $mdMedia('xs'); }, function (xs) {
            model.xs = xs;
            model.enablePopoverAutocomplete = model.xs || (model.iOS && !model.gtMd);
        });
        $scope.$watch(function () { return $mdMedia('gt-md'); }, function (gtMd) {
            model.gtMd = gtMd;
            model.enablePopoverAutocomplete = model.xs || (model.iOS && !model.gtMd);
        });


        /* ==============================
                   Public Methods
           ============================== */

        // this question is disabled if the survey's current index is not equal to the questions index 
        model.isDisabled = function () {
            return model.currentIndex !== model.index;
        };

        // method to render secure HTML
        model.renderHtml = function (html) {
            return $sce.trustAsHtml(html);
        }

        // custom input validation triggered by ng-blur
        // value: the input value
        // ngModel: the input ngModelController
        model.validate = function (value, ngModel) {
            // validate uniqueness only when the ngModel is valid so that we don't waste bandwidth calling the unique validation endpoint
            if (model.data.uniqueValidation && (ngModel.$valid || ngModel.$error["unique"])) {
                model._timeout = true;
                $timeout(function () {
                    validateIsUnique(value, ngModel);
                }, 450);
            }
        }

        // checks if the next question button is enabled
        // will fail if a custom validation is in progress
        model.nextEnabled = function(form) {
            return !model._timeout && ((!model.isDisabled() && model.isValid(form)) || (!model.isDisabled() && model.data.optional));
        }

        model.next = function (questionIndex, answer) {
            // do nothing when a custom validation is in progress as there will be a delay in updating the visibility of the next button
            if (!model._timeout) {
                model.nextQuestion({ questionIndex: questionIndex, answer: answer });
            }
        };

        /* ==============================
                   Internal Methods
           ============================== */

        // search dialog
        function openSearchDialog(ev) {
            $mdDialog.show({
                controller: ['$scope', '$mdDialog', function ($scope, $mdDialog) {
                    $scope.querySearch = model.querySearch;
                    $scope.searchText = "";
                    $scope.answer = null;
                    $scope.add = function (answer) {
                        $mdDialog.hide(answer);
                    };
                    $scope.cancel = function () {
                        $mdDialog.cancel();
                    };
                }],
                template: "<md-dialog>\
                            <form ng-cloak>\
                                <md-autocomplete style='min-width: 300px;'\
                                    md-selected-item='answer'\
                                    md-search-text='searchText'\
                                    md-items='item in querySearch(searchText)'\
                                    md-item-text='item'\
                                    md-min-length='0'\
                                    md-input-name='innerInput'\
                                    md-autofocus\
                                    md-require-match='false'>\
                                    <md-item-template>\
                                        <span md-highlight-text='searchText'\
                                                md-highlight-flags='^i'>\
                                            {{item}}\
                                        </span>\
                                    </md-item-template>\
                                </md-autocomplete>\
                                <md-dialog-actions layout='row'>\
                                    <md-button ng-click='cancel()'>\
                                        Cancel\
                                    </md-button>\
                                    <md-button ng-disabled='searchText == \"\"'\
                                            ng-click='add(searchText)'>\
                                        Add\
                                    </md-button>\
                                </md-dialog-actions>\
                            </form>\
                        </md-dialog>",
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: true
            }).then(function (answer) {
                if (model.answer.indexOf(answer) === -1) {
                    model.answer.push(answer);
                }
            }, function () {
            });
        }

        // validate if an ngModel has unique value by calling a configured endpoint
        function validateIsUnique(value, ngModel) {
            var url = model.data.uniqueValidation.url.replace(/%(.*?)%/, function (a, b) {
                return value;
            });

            // call the endpoint
            $http.get(url).then(function(response) {
                ngModel.$setValidity('unique', response.data.isUnique);
                model._timeout = false;
            }, function(error) {
                console.log(error);
                ngModel.$setValidity('unique', true);
                model._timeout = false;
            });
        }

        // method called by md-chips - md-chips is used by autocompletemultiple question type
        function transformChip(chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

            // Otherwise, create a new one
            return chip;
        }

        // method called by md-autocomplete - md-autocomplete is used by autocomplete and autocompletemultiple question types
        function querySearch(query) {
            var results = model.nodes ? model.nodes.filter(createFilterFor(query)) : [],
                deferred;
            if (model.simulateQuery) {
                deferred = $q.defer();
                $timeout(function () { deferred.resolve(results); }, Math.random() * 1000, false);
                return deferred.promise;
            } else {
                return results;
            }
        }

        // placeholder for future autocomplete search text change callback functionality
        function searchTextChange(text) {
        }

        // placeholder for future autocomplete selected item change callback functionality
        function selectedItemChange(item) {
        }

        // search algorithm for autocomplete and autocompletemultiple question types
        function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);
            return function filterFn(node) {
                var regex = new RegExp(lowercaseQuery);
                return (angular.lowercase(node).match(regex));
            };
        }

        // placeholder for creating new data when autocomplete or autocompletemultiple search returns no matching result
        function newSearchNode(node) {
        }
    }];

var template = "";
template += "<div flex=\"none\"";
template += "     hide-xs>";
template += "    <h3>";
template += "        <i class=\"fa fa-long-arrow-right\"";
template += "            aria-hidden=\"true\"><\/i>";
template += "    <\/h3>";
template += "<\/div>";
template += "<ng-form name=\"innerForm\"";
template += "            flex=\"nogrow\"";
template += "            class=\"chip\"";
template += "            aria-hidden=\"true\">";
template += "";
template += "    <h3 ng-bind-html=\"model.renderHtml(model.title)\"";
template += "        style=\"font-size: 24px\"><\/h3>";
template += "    <br \/>";
template += "    <br \/>";
template += "    <info ng-if=\"model.data.hint\">";
template += "        <i class=\"fa fa-info\"";
template += "            aria-hidden=\"true\">";
template += "        <\/i> {{model.data.hint}}";
template += "        <br \/>";
template += "        <br \/>";
template += "    <\/info>";
template += "";
template += "    <!-- Autocomplete -->";
template += "    <div ng-if=\"model.data.type == 'autocomplete'\">";
template += "        <md-autocomplete ng-disabled=\"model.isDisabled()\"";
template += "                         md-no-cache=\"model.data.noCache\"";
template += "                         md-selected-item=\"model.answer\"";
template += "                         md-search-text-change=\"model.searchTextChange(model.searchText)\"";
template += "                         md-search-text=\"model.searchText\"";
template += "                         md-selected-item-change=\"model.selectedItemChange(item)\"";
template += "                         md-items=\"item in model.querySearch(model.searchText)\"";
template += "                         md-item-text=\"item\"";
template += "                         md-min-length=\"0\"";
template += "                         md-input-name=\"innerInput\"";
template += "                         ng-attr-ng-required=\"{{ model.data.optional ? 'false' : 'true' }}\">";
template += "            <md-item-template>";
template += "                <span md-highlight-text=\"model.searchText\"";
template += "                        md-highlight-flags=\"^i\">";
template += "                    {{item}}";
template += "                <\/span>";
template += "            <\/md-item-template>";
template += "            <md-not-found>";
template += "                No search result matching \"{{model.searchText}}\" were found.";
template += "                <a ng-click=\"model.newSearchNode(model.searchText)\">Create a new one!<\/a>";
template += "            <\/md-not-found>";
template += "        <\/md-autocomplete>";
template += "        <br \/>";
template += "    <\/div>";
template += "";
template += "    <!-- Autocomplete multiple selection -->";
template += "    <div ng-if=\"model.data.type == 'autocompletemultiple' && !model.enablePopoverAutocomplete\">";
template += "        <md-chips ng-model=\"model.answer\"";
template += "                  md-autocomplete-snap";
template += "                  md-require-match=\"model.data.requireMatch\"";
template += "                  md-max-chips=\"{{model.data.max}}\"";
template += "                  ng-disabled=\"model.isDisabled()\"";
template += "                  flex=\"nogrow\">";
template += "            <md-autocomplete ng-disabled=\"model.isDisabled()\"";
template += "                             md-selected-item=\"model.selectedItem\"";
template += "                             md-require-match=\"model.data.requireMatch\"";
template += "                             md-autoselect=\"true\"";
template += "                             md-no-cache=\"true\"";
template += "                             md-select-on-match=\"true\"";
template += "                             md-search-text=\"model.searchText\"";
template += "                             md-items=\"item in model.querySearch(model.searchText)\"";
template += "                             md-item-text=\"item\"";
template += "                             md-min-length=\"0\"";
template += "                             md-input-name=\"innerInput\"";
template += "                             ng-attr-ng-required=\"{{ model.data.optional ? 'false' : 'true' }}\">";
template += "                <span md-highlight-text=\"model.searchText\">{{item}}<\/span>";
template += "            <\/md-autocomplete>";
template += "            <md-chip-template>";
template += "                <span>{{$chip}}<\/span>";
template += "            <\/md-chip-template>";
template += "        <\/md-chips>";
template += "        <br \/>";
template += "    <\/div>";
template += "    <div ng-if=\"model.data.type == 'autocompletemultiple' && model.enablePopoverAutocomplete\">";
template += "        <md-chips ng-disabled=\"model.isDisabled()\"";
template += "                  ng-model=\"model.answer\"";
template += "                  flex=\"nogrow\">";
template += "            <input type=\"text\"";
template += "                   class=\"popover-autocomplete\"";
template += "                   readonly";
template += "                   ng-click=\"model.openSearchDialog($event)\">";
template += "            <md-chip-template>";
template += "                <span>{{$chip}}<\/span>";
template += "            <\/md-chip-template>";
template += "        <\/md-chips>";
template += "        <br \/>";
template += "    <\/div>";
template += "";
template += "    <!-- Yes or no -->";
template += "    <div ng-if=\"model.data.type == 'yesorno'\">";
template += "        <md-radio-group name=\"innerInput\"";
template += "                        ng-model=\"model.answer\"";
template += "                        ng-attr-ng-required=\"{{ model.data.optional ? 'false' : 'true' }}\">";
template += "            <md-radio-button value=\"Yes\"";
template += "                                class=\"md-primary\"";
template += "                                ng-disabled=\"model.isDisabled()\">";
template += "                Yes";
template += "            <\/md-radio-button>";
template += "            <md-radio-button value=\"No\"";
template += "                                ng-disabled=\"model.isDisabled()\">";
template += "                No";
template += "            <\/md-radio-button>";
template += "        <\/md-radio-group>";
template += "    <\/div>";
template += "";
template += "    <!-- Multiple choice (single answer) -->";
template += "    <div ng-if=\"model.data.type == 'multiplechoice'\">";
template += "        <md-radio-group name=\"innerInput\"";
template += "                        ng-model=\"model.answer\"";
template += "                        ng-attr-ng-required=\"{{ model.data.optional ? 'false' : 'true' }}\">";
template += "            <md-radio-button ng-repeat=\"choice in model.data.choices\"";
template += "                                value=\"{{choice.value}}\"";
template += "                                ng-disabled=\"model.isDisabled()\">";
template += "                {{choice.text}}";
template += "            <\/md-radio-button>";
template += "        <\/md-radio-group>";
template += "    <\/div>";
template += "";
template += "    <!-- Single text answer -->";
template += "    <div ng-if=\"model.data.type == 'singletext'\">";
template += "        <md-input-container class=\"md-block\">";
template += "            <label>{{model.data.label}}<\/label>";
template += "            <input type=\"text\"";
template += "                   ng-disabled=\"model.isDisabled()\"";
template += "                   ng-model=\"model.answer\"";
template += "                   name=\"innerInput\"";
template += "                   ng-attr-ng-required=\"{{ model.data.optional ? 'false' : 'true' }}\">";
template += "            <div ng-messages=\"innerForm.innerInput.$error\"";
template += "                 ng-show=\"innerForm.innerInput.$invalid && innerForm.innerInput.$dirty\">";
template += "                <div ng-message=\"required\">This is required!<\/div>";
template += "            <\/div>";
template += "        <\/md-input-container>";
template += "    <\/div>";
template += "";
template += "    <!-- Fullname answer -->";
template += "    <div ng-if=\"model.data.type == 'fullname'\"";
template += "            layout=\"row\"";
template += "            layout-xs=\"column\">";
template += "        <md-input-container class=\"md-block\">";
template += "            <label>First<\/label>";
template += "            <input type=\"text\"";
template += "                    ng-disabled=\"model.isDisabled()\"";
template += "                    ng-model=\"model.answer[0]\"";
template += "                    name=\"innerInput_0\"";
template += "                    ng-attr-ng-required=\"{{ model.data.optional ? 'false' : 'true' }}\">";
template += "            <div ng-messages=\"innerForm.innerInput_0.$error\"";
template += "                    ng-show=\"innerForm.innerInput_0.$invalid && innerForm.innerInput_0.$dirty\">";
template += "                <div ng-message=\"required\">This is required!<\/div>";
template += "            <\/div>";
template += "        <\/md-input-container>";
template += "        <md-input-container class=\"md-block\">";
template += "            <label>Last<\/label>";
template += "            <input type=\"text\"";
template += "                    ng-disabled=\"model.isDisabled()\"";
template += "                    ng-model=\"model.answer[1]\"";
template += "                    name=\"innerInput_1\"";
template += "                    ng-attr-ng-required=\"{{ model.data.optional ? 'false' : 'true' }}\">";
template += "            <div ng-messages=\"innerForm.innerInput_1.$error\"";
template += "                    ng-show=\"innerForm.innerInput_1.$invalid && innerForm.innerInput_1.$dirty\">";
template += "                <div ng-message=\"required\">This is required!<\/div>";
template += "            <\/div>";
template += "        <\/md-input-container>";
template += "    <\/div>";
template += "";
template += "    <!-- Number answer -->";
template += "    <div ng-if=\"model.data.type == 'number'\">";
template += "        <md-input-container class=\"md-block\">";
template += "            <label>{{model.data.label}}<\/label>";
template += "            <input type=\"number\"";
template += "                   step=\"{{model.data.step}}\"";
template += "                   ng-disabled=\"model.isDisabled()\"";
template += "                   ng-model=\"model.answer\"";
template += "                   name=\"innerInput\"";
template += "                   min=\"{{model.data.min}}\"";
template += "                   max=\"{{model.data.max}}\"";
template += "                   ng-attr-ng-required=\"{{ model.data.optional ? 'false' : 'true' }}\">";
template += "            <div ng-messages=\"innerForm.innerInput.$error\"";
template += "                    ng-show=\"innerForm.innerInput.$invalid && innerForm.innerInput.$dirty\">";
template += "                <div ng-message=\"required\">This is required!<\/div>";
template += "                <div ng-message=\"min\">Minimum is {{model.data.min}}!<\/div>";
template += "                <div ng-message=\"max\">Maximum is {{model.data.max}}!<\/div>";
template += "            <\/div>";
template += "        <\/md-input-container>";
template += "    <\/div>";
template += "";
template += "    <!-- Money answer -->";
template += "    <div ng-if=\"model.data.type == 'money'\">";
template += "        <md-input-container class=\"md-block\">";
template += "            <label>{{model.data.label}}<\/label>";
template += "            <md-icon md-font-icon=\"fa fa-usd\"><\/md-icon>";
template += "            <input type=\"number\"";
template += "                    step=\"{{model.data.step}}\"";
template += "                    ng-disabled=\"model.isDisabled()\"";
template += "                    ng-model=\"model.answer\"";
template += "                    name=\"innerInput\"";
template += "                    min=\"0\"";
template += "                    ng-attr-ng-required=\"{{ model.data.optional ? 'false' : 'true' }}\">";
template += "            <div ng-messages=\"innerForm.innerInput.$error\"";
template += "                    ng-show=\"innerForm.innerInput.$invalid && innerForm.innerInput.$dirty\">";
template += "                <div ng-message=\"required\">This is required!<\/div>";
template += "            <\/div>";
template += "        <\/md-input-container>";
template += "    <\/div>";
template += "";
template += "    <!-- Email answer -->";
template += "    <div ng-if=\"model.data.type == 'email'\">";
template += "        <md-input-container class=\"md-block\">";
template += "            <label>{{model.data.label}}<\/label>";
template += "            <md-icon>email<\/md-icon>";
template += "            <input type=\"email\"";
template += "                    ng-disabled=\"model.isDisabled()\"";
template += "                    ng-model=\"model.answer\"";
template += "                    name=\"innerInput\"";
template += "                    ng-attr-ng-required=\"{{ model.data.optional ? 'false' : 'true' }}\"";
template += "                    ng-pattern=\"\/^.+@.+\..+$\/\"";
template += "                    ng-change=\"model.validate(model.answer, innerForm.innerInput)\"";
template += "                    ng-model-options=\"{debounce: 750}\">";
template += "            <div ng-messages=\"innerForm.innerInput.$error\"";
template += "                    ng-show=\"innerForm.innerInput.$invalid && innerForm.innerInput.$dirty\">";
template += "                <div ng-message-exp=\"['required', 'pattern']\">";
template += "                    Your email must look like an email address.";
template += "                <\/div>";
template += "                <div ng-message=\"unique\">";
template += "                    This email address has already been taken.";
template += "                <\/div>";
template += "            <\/div>";
template += "        <\/md-input-container>";
template += "    <\/div>";
template += "";
template += "    <!-- Textarea answer -->";
template += "    <div ng-if=\"model.data.type == 'textarea'\">";
template += "        <md-input-container class=\"md-block\">";
template += "            <label>{{model.data.label}}<\/label>";
template += "            <textarea ng-disabled=\"model.isDisabled()\"";
template += "                        ng-model=\"model.answer\"";
template += "                        name=\"innerInput\"";
template += "                        required";
template += "                        rows=\"{{model.data.rows}}\"";
template += "                        md-maxlength=\"{{model.data.maxlength}}\"><\/textarea>";
template += "            <div ng-messages=\"innerForm.innerInput.$error\"";
template += "                    ng-show=\"innerForm.innerInput.$invalid && innerForm.innerInput.$dirty\">";
template += "                <div ng-message=\"required\">This is required!<\/div>";
template += "            <\/div>";
template += "        <\/md-input-container>";
template += "    <\/div>";
template += "";
template += "    <div>";
template += "        <button ng-if=\"model.nextEnabled(innerForm)\"";
template += "                ng-click=\"model.nextQuestion({questionIndex: model.currentIndex, answer: model.answer})\"";
template += "                type=\"button\"";
template += "                class=\"btn btn-default next\">";
template += "            <i class=\"fa fa-check\" aria-hidden=\"true\"><\/i>";
template += "            <span ng-if=\"model.isLastQuestion\">";
template += "                Submit";
template += "            <\/span>";
template += "        <\/button>";
template += "    <\/div>";
template += "    <md-progress-circular ng-if=\"model.deferNextEnabled\"";
template += "                            class=\"md-hue-2\"";
template += "                            md-diameter=\"20px\">";
template += "    <\/md-progress-circular>";
template += "<\/ng-form>";

module.exports = {
    template: template,
    controller: questionController,
    controllerAs: 'model',
    bindings: {
        data: '<',
        nextQuestion: '&',
        currentIndex: '<',
        isLastQuestion: '<',
        index: '<',
        title: '<qTitle'
    }
};