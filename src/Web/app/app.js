﻿require('./components/survey/Module.js');

var vendorController = require('./controllers/VendorController.js')
var homeController = require('./controllers/HomeController.js')
var surveyService = require('./services/SurveyService.js')
var subscriberService = require('./services/SubscriberService.js')
var vendorService = require('./services/VendorService.js')
var ipifyService = require('./services/IpifyService.js')

var app = angular.module('LumavenFrontend', ['ngMaterial', 'ngMessages', 'survey'])

app.controller('VendorController', vendorController);
app.controller('HomeController', homeController);
app.factory('SurveyService', surveyService);
app.factory('SubscriberService', subscriberService);
app.factory('VendorService', vendorService);
app.factory('IpifyService', ipifyService);