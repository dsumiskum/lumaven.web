﻿module.exports = [
    '$scope',
    'SurveyService',
    'VendorService',
    '$timeout',
    '$q',
    'IpifyService',
    function ($scope, surveyService, vendorService, $timeout, $q, ipifyService) {
        var model = this;

        surveyService.getConfig()
            .then(function (response) {
                model.surveyConfig = response.data;
            }, function (error) {
                console.log(error);
            });

        ipifyService.getIp().then(function (response) {
            model.ipAddress = response.data;
        }, function (error) {
            console.log(error);
        });

        model.submitVendor = function (answers) {
            var deferred = $q.defer();

            $timeout(function () {
                deferred.notify("POST");
                vendorService.add(answers, model.ipAddress)
                .then(function (response) {
                    deferred.resolve(response);
                }, function (error) {
                    deferred.reject(error);
                });
            })

            return deferred.promise;
        }
    }];