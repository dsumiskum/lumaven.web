﻿module.exports = [
    'SubscriberService',
    'IpifyService',
    function (subscriberService, ipifyService) {
        var model = this;
        $(".success-indicator").hide();

        ipifyService.getIp().then(function (response) {
            model.ipAddress = response.data;
        }, function (error) {
            console.log(error);
        });

        model.notify = function (viewModel) {
            var email = { "Email": viewModel.$viewValue };

            if (!viewModel.$invalid) {
                console.log("POST " + email);
                subscriberService.add(email, model.ipAddress).success(function (data) {
                    console.log(data);
                    model.success = true;
                    $(".success-indicator").show(300);
                });
            }
        };
}]