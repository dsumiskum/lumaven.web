﻿module.exports = ['$http', function ($http) {
    var factory = {};

    factory.add = function (vendorInfo, ipAddress) {
        return $http({
            method: 'POST',
            url: '/api/vendor',
            headers: {
                'Content-Type': 'application/json',
                'IpAddress': ipAddress
            },
            data: vendorInfo
        });
    }

    return factory;
}]