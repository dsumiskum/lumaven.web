﻿module.exports = ['$http', function ($http) {
    var factory = {};

    factory.getConfig = function () {
        return $http.get('/vendor.json');
    }

    return factory;
}]