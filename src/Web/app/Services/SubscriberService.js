﻿module.exports = ['$http', function ($http) {
    var factory = {};

    factory.add = function (email, ipAddress) {
        return $http({
            method: 'POST',
            url: '/api/subscriber',
            headers: {
                'Content-Type': 'application/json',
                'IpAddress': ipAddress
            },
            data: email
        });
    }

    return factory;
}]