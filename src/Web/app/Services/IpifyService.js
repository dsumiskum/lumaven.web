﻿module.exports = ['$http', function ($http) {
    var factory = {};

    factory.getIp = function () {
        return $http({
            method: 'GET',
            url: 'https://api.ipify.org'
        });
    }

    return factory;
}]